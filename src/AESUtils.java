public class AESUtils {

    private static String seed = "";

    public static void init(String seed) {
        AESUtils.seed = seed;
    }

    public static String encryption(String strNormalText){
        String seedValue = AESUtils.seed;
        String normalTextEnc = "";
        try {
            normalTextEnc = AESHelper.encrypt(seedValue, strNormalText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return normalTextEnc;
    }

    public static String decryption(String strEncryptedText){
        String seedValue = AESUtils.seed;
        String strDecryptedText = "";
        try {
            strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDecryptedText;
    }
}
