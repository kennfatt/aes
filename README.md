# AES Framework #

Java source code to encrypt and decrypt String using Cipher method.

## Requirements: ##
- Latest version of Java
- Any platform

## Usage ##

First step, Initiate the main class:
```java
AESUtils.init(String passwordToEncrypt);
```
Use `passwordToEncrypt` to encrypt and decrypt string

Second step, Encrypting and Decrypting:
```java
String encryptedText = AESUtils.encryption(String textToEncrypt);
String decryptedText = AESUtils.decryption(encryptedText);
```

## About ##
Special thanks to [varotariya vajsi](https://stackoverflow.com/users/2897365/varotariya-vajsi) as author of these codes.  
Also thanks to [Stack Overflow](https://stackoverflow.com/) as active programming forum.

